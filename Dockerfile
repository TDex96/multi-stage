FROM ubuntu AS stage1
WORKDIR /root
RUN touch stagefile1

FROM alpine AS stage2
WORKDIR /root
RUN touch stagefile2

FROM alpine
COPY --from=stage1 /root/stagefile1 /root/stagefile1
COPY --from=stage2 /root/stagefile2 /root/stagefile2
